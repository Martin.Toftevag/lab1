package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    public String random_choice(){
        Random rand = new Random();
        String randomElement = rpsChoices.get(rand.nextInt(rpsChoices.size()));
        return randomElement;
    }
    public String user_choice(){
        while(true){
            System.out.println("Your choice (Rock/Paper/Scissors)?");
            String human_choice = sc.next().toLowerCase();
            if(rpsChoices.contains(human_choice)){
                return human_choice;
            }else{
                System.out.println("i don't understand " +human_choice + " Try again");
            }
            }
        }
    public String continue_playing(){
        while(true){
            System.out.println("Do you wish to continue playing? (y/n)?");
            String continue_answer = sc.next().toLowerCase();
            if(continue_answer.length()==1){
                if( continue_answer.charAt(0) == 'y'){
                    return continue_answer;
                }else if(continue_answer.charAt(0) == 'n'){
                    return continue_answer;
                }else{
                    System.out.println("I don't understand"+continue_answer + ". Could you try again?");
                }
            }
        }
    }/*
    public Boolean is_winner(String choice1,String choice2){
        if(choice1 == "paper"){
            return choice2 =="rock";
        }else if(choice1 == "scissors"){
            return choice2 =="paper";
        }else{
            return choice2 == "scissors";
        }
    }*/
    public void run() {
        // TODO: Implement Rock Paper Scissors
        while(true){
            System.out.println("Let's play round "+roundCounter);
            roundCounter++;
            String human_choice = user_choice().toLowerCase();
            String computer_choice = random_choice().toLowerCase();
            String choice_string = "Human chose "+human_choice+", computer chose " + computer_choice + ".";
            if(human_choice.equals(computer_choice)){
                System.out.println(choice_string + ". It's a tie.");
            }else if(human_choice.equals("rock") && computer_choice.equals("scissors")){
                System.out.println(choice_string+". Human wins. ");
                humanScore++;
            }else if(human_choice.equals("rock") && computer_choice.equals("paper")){
                System.out.println(choice_string+". Computer wins.");
                computerScore++;
            }else if(human_choice.equals("paper") && computer_choice.equals("rock")){
                System.out.println(choice_string+". Human wins.");
                humanScore++;
            }else if(human_choice.equals("paper") && computer_choice.equals("scissors")){
                System.out.println(choice_string+". Computer wins.");
                computerScore++;
            }else if(human_choice.equals("scissors") && computer_choice.equals("paper")){
                System.out.println(choice_string+". Human wins!");
                humanScore++;
            }else{
                System.out.println(choice_string+". Computer wins!");
                computerScore++;
            }
            
            System.out.println("Score: human "+humanScore+", computer "+computerScore);
            
            String continue_answer = continue_playing();
            if(continue_answer.charAt(0) == 'n'){
                break;
            }
            
        }System.out.println("Bye bye :)");
    } 
    

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }
    }